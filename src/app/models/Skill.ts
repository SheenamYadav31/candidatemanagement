export class Skill{
	id: number;
	name: string;
	createdBy: string;
	createdOn: Date;
}