export class Graduate{
	id: number;
	name: string;
	email: string;
	institute: string;
	degree: string;
	address: string;
	location: string;
	feedback: string;
	joiningDate: Date;
	creator: string;
	skills: number[];
}