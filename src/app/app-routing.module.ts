import { NgModule } from '@angular/core';
import { Routes, CanActivate, RouterModule, RouterLink} from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AddgradComponent } from './components/addgrad/addgrad.component';
import { EditgradComponent } from './components/editgrad/editgrad.component';
import { AddskillComponent } from './components/addskill/addskill.component';
import { TrendsComponent } from './components/trends/trends.component';
import { ViewgradComponent } from './components/viewgrad/viewgrad.component';
import { ViewskillComponent } from './components/viewskill/viewskill.component';
import { DisplaygradComponent } from './components/displaygrad/displaygrad.component';
import { GraduatehistoryComponent } from './components/graduatehistory/graduatehistory.component';

import { AuthGuardService as AuthGuard } from './providers/auth/auth-guard.service';

export const routes: Routes = [
	{
		path: '',
		redirectTo: 'login',
		pathMatch: 'full'	
	},
	{
		path: 'login',
		component: LoginComponent,
		data: {
			title: 'Login Page'
		}	
	},
	{
		path: 'dashboard',
		component: DashboardComponent,
		data: {
			title: 'Dashboard Page'
		},
		canActivate: [AuthGuard]	
	},
	{
		path: 'addgrad',
		component: AddgradComponent,
		data:{
			title: 'Add New Graduate'
		},
		canActivate: [AuthGuard]
	},
	{
		path: 'updategrad',
		component: EditgradComponent,
		data:{
			title: 'Update Graduate'
		},
		canActivate: [AuthGuard]
	},
	{
		path: 'addskill',
		component: AddskillComponent,
		data:{
			title: 'Add New Skill'
		},
		canActivate: [AuthGuard]
	},
	{
		path: 'viewgrad',
		component: ViewgradComponent,
		data:{
			title: 'Graduates'
		},
		canActivate: [AuthGuard]
	},
	{
		path: 'viewskill',
		component: ViewskillComponent,
		data:{
			title: 'Skills'
		},
		canActivate: [AuthGuard]
	},
	{
		path: 'Graduate',
		component: DisplaygradComponent,
		data:{
			title: 'Graduate Record'
		},
		canActivate: [AuthGuard]
	},
	{
		path: 'trends',
		component: TrendsComponent,
		data:{
			title: 'Trends'
		},
		canActivate: [AuthGuard]
	},
	{
		path: 'history',
		component: GraduatehistoryComponent,
		data:{
			title: 'History'
		},
		canActivate: [AuthGuard]
	},
	{
		path: "**",
		component:NotFoundComponent
	}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
