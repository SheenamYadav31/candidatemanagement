import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TrendsService {
  url;
  constructor(private http: HttpClient) { }
  API_URL= environment.API_URL

  //GET
  GetLocationList(){
    this.url = this.API_URL.concat("/trends/locations");
  	return this.http.get(this.url);
  }
  //POST
  GetGradCountByLocation(response){
    this.url = this.API_URL.concat("/trends/graduatesfromLocation");
  	return this.http.post(this.url, response);
  }

  //GET
  GetDegreeList(){
    this.url = this.API_URL.concat("/trends/degrees");
  	return this.http.get(this.url);
  }
  //POST
  GetGradCountByDegree(response){
    this.url = this.API_URL.concat("/trends/graduatesfromDegree");
  	return this.http.post(this.url, response);
  }

  //GET
  GetJoiningYearList(){
    this.url = this.API_URL.concat("/trends/joiningyears");
  	return this.http.get(this.url);
  }
  //POST
  GetGradCountByJoiningYear(response){
    this.url = this.API_URL.concat("/trends/graduatesfromYear");
  	return this.http.post(this.url, response);
  }

  //GET
  GetInstituteList(){
    this.url = this.API_URL.concat("/trends/institutes");
  	return this.http.get(this.url);
  }
  //POST
  GetGradCountByInstitute(response){
    this.url = this.API_URL.concat("/trends/graduatesfromInstitute");
  	return this.http.post(this.url, response);
  }
}
