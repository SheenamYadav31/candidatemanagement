import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GraduateService {
  url;
  constructor(private http: HttpClient) { }

  API_URL= environment.API_URL

  //GET
  ViewGraduate(){
  	this.url = this.API_URL.concat("/graduate/all");
    // this.url = "http://localhost:8080/graduate/all";
  	return this.http.get(this.url);
  }
  //POST
  SaveGraduate(response){
    this.url = this.API_URL.concat("/graduate/add");
  	return this.http.post(this.url, response);
  }
  //POST
  UpdateGraduate(response){
    this.url = this.API_URL.concat("/graduate/update");
    return this.http.post(this.url, response);
  }
  //POST
  GetGraduateHistory(response){
    this.url = this.API_URL.concat("/graduate/versionhistory");
    return this.http.post(this.url, response);
  }
}
