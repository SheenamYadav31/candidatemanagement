import { TestBed } from '@angular/core/testing';

import { GraduateService } from './graduate.service';

describe('GraduateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GraduateService = TestBed.get(GraduateService);
    expect(service).toBeTruthy();
  });
});
