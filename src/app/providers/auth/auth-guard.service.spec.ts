import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { AuthGuardService } from './auth-guard.service';

describe('AuthGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthGuardService = TestBed.get(AuthGuardService);
    // spyOn(service.canActivate).and.returnValue(of(false));
    service.canActivate();
    expect(false).toBeFalsy();
  });
});