import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from 'src/app/providers/authentication/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{

  constructor(public auth: AuthService, public router: Router) { }

  canActivate(): boolean {
    if (this.auth.loggedIn()) {
      return true;
    }
    else{
    	this.router.navigate(['/login']);
    	return false;
    }
  }
} 