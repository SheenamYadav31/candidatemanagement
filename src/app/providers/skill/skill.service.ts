import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SkillService {
  url;
  constructor(private http: HttpClient) { }
  API_URL= environment.API_URL
  //GET
  ViewSkill(){
    this.url = this.API_URL.concat("/skill/all");
  	return this.http.get(this.url);
  }

  SaveSkill(response){
    this.url = this.API_URL.concat("/skill/add");
  	return this.http.post(this.url, response);
  }
}
