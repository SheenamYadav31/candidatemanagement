import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { DebugElement } from '@angular/core'; 
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router, Routes, RouterModule, ActivatedRoute, Params } from '@angular/router';  
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { BrowserModule, By } from '@angular/platform-browser';
import { of } from 'rxjs';
import {Graduate} from 'src/app/models/Graduate';
import {Skill} from 'src/app/models/Skill';
import { GraduateService } from 'src/app/providers/graduate/graduate.service';    
import { SkillService } from 'src/app/providers/skill/skill.service';    
import { Socialusers } from 'src/app/models/socialusers';
import { AddgradComponent } from './addgrad.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AuthGuardService as AuthGuard } from 'src/app/providers/auth/auth-guard.service';

import { DashboardComponent } from 'src/app/components/dashboard/dashboard.component';
import { NotFoundComponent } from 'src/app/components/not-found/not-found.component';

describe('AddgradComponent', () => {
  let component: AddgradComponent;
  let fixture: ComponentFixture<AddgradComponent>;
  let skillService: SkillService;
  let graduateService: GraduateService;
  let de: DebugElement;
  let el: HTMLElement;

  const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: {
      title: 'Dashboard Page'
    },
    canActivate: [AuthGuard]  
  },
  {
    path: 'addgrad',
    component: AddgradComponent,
    data:{
      title: 'Add New Graduate'
    },
    canActivate: [AuthGuard]
  }
];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddgradComponent, DashboardComponent ],
      imports : [
        RouterModule.forRoot(routes),
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule
      ],
      providers: [ GraduateService, SkillService ]
    })
    .compileComponents().then(() => {
      fixture = TestBed.createComponent(AddgradComponent);
      component = fixture.componentInstance;
      de = fixture.debugElement.query(By.css('form'));
      el = de.nativeElement;
    });
    skillService = TestBed.get(SkillService);
    graduateService = TestBed.get(GraduateService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddgradComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    const dummySkills = [
      {
        id: 1,
        name: 'Python',
        createdOn: new Date(),
        createdBy: 'testUserID',
      },
      {
        id: 2,
        name: 'Java',
        createdOn: new Date(),
        createdBy: 'testUserID',
      }
    ];

    spyOn(skillService, 'ViewSkill').and.returnValue(of(dummySkills));
    component.ngOnInit();
    expect(component.allskills).toBe(dummySkills);
    // expect(skillService.ViewSkill).toHaveBeenCalled();
  });

  it('should add graduate', ()=>{
    const dummyGrad = {
      id: 1,
      name: 'Sheenam',
      email: 'sheenam@testemail.com',
      institute: 'University of Delhi',
      degree: 'MCA',
      address: 'Delhi, India',
      location: 'Bangalore',
      feedback: 'Great!!',
      joiningDate: new Date('2020-06-01'),
      creator: 'testUserID',
      skills: [1,2]
    };
    spyOn(component.form, 'value').and.returnValue(of(dummyGrad));
    spyOn(component.form.get('skills'), 'value').and.returnValue(of(dummyGrad));
    component.onSubmit();
    expect(component.newgrad).toBe(new Graduate());
  });
});
