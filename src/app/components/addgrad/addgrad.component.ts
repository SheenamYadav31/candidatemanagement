import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 
import { Validators, FormGroup, FormControl } from '@angular/forms';

import {Graduate} from 'src/app/models/Graduate';
import {Skill} from 'src/app/models/Skill';
import { GraduateService } from 'src/app/providers/graduate/graduate.service';    
import { SkillService } from 'src/app/providers/skill/skill.service';    
import { Socialusers } from 'src/app/models/socialusers';


@Component({
  selector: 'app-addgrad',
  templateUrl: './addgrad.component.html',
  styleUrls: ['./addgrad.component.css']
})
export class AddgradComponent implements OnInit {

  newgrad = new Graduate();
  user;  
  form = new FormGroup({
  	name: new FormControl('', [Validators.required, Validators.minLength(3)] ),
  	email: new FormControl('', Validators.required),
  	institute: new FormControl(''),
  	location: new FormControl(''),
    degree: new FormControl(''),
    joiningdate: new FormControl(''),
    address: new FormGroup({
      addressText: new FormControl(''),
    }),
    skills : new FormGroup({
      sname: new FormControl('')
    }),
    feedback: new FormControl('')
  });

  allskills: Skill[] = [];
  gradSkills: number[] = [];

  constructor(private graduateService: GraduateService, private skillService: SkillService, private router: Router) { }

  ngOnInit() {
  	this.skillService.ViewSkill().subscribe((skillList : Skill[]) => {
  		console.log("skillList : ");
  		this.allskills = [...skillList];
  		console.log(this.allskills);
  	});
  }

  addToSkillList(){
  	//adding skill id to the list of grad skills
  	this.gradSkills.push(this.form.controls['skills'].value.sname);
  	console.log(this.gradSkills);
  }

  GetUser(){
    this.user = localStorage.getItem('socialusers');
    console.log("creator of the Graduate: ")
    console.log(this.user);
    let result = JSON.parse(window.localStorage.getItem('socialusers'));
    console.log(result);
    return result.id;
  }

  onSubmit(){

    console.log(this.form.value.address.addressText);
    this.newgrad.name = this.form.value.name;
    this.newgrad.email = this.form.value.email;
    this.newgrad.institute = this.form.value.institute;
    this.newgrad.location = this.form.value.location;
    this.newgrad.degree = this.form.value.degree;
    this.newgrad.joiningDate = this.form.value.joiningdate;
    this.newgrad.address = this.form.controls['address'].value.addressText;
    this.newgrad.skills =this.gradSkills;
    this.newgrad.creator = this.GetUser();
    this.newgrad.feedback = this.form.value.feedback;
    this.saveGrad(this.newgrad);
  }

  saveGrad(grad: Graduate) {  
   this.graduateService.SaveGraduate(grad).subscribe((res:any) => {
     console.log(res);
     // this.response = res.details;
     localStorage.setItem('newGrad', JSON.stringify(this.newgrad));
     console.log(localStorage.setItem('newGrad', JSON.stringify(this.newgrad)));
     this.router.navigate(['/dashboard']);
   });  
  }  

}
