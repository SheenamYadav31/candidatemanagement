import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 
import {  Validators, FormGroup, FormControl } from '@angular/forms';

import {Skill} from 'src/app/models/Skill';
import { SkillService } from 'src/app/providers/skill/skill.service';    
import { Socialusers } from 'src/app/models/socialusers';

@Component({
  selector: 'app-addskill',
  templateUrl: './addskill.component.html',
  styleUrls: ['./addskill.component.css']
})
export class AddskillComponent implements OnInit {

  constructor(private skillService: SkillService, private router: Router) { }

  newSkill = new Skill();
  user;  
  form = new FormGroup({
  	// skillname: new FormControl(''),
  	skillid: new FormControl(''),
  });

  ngOnInit() {
  }

  GetUser(){
    this.user = localStorage.getItem('socialusers');
    console.log("creator of the Skill: ")
    console.log(this.user);
    let result = JSON.parse(window.localStorage.getItem('socialusers'));
    console.log(result);
    return result.id;
  }

  onSubmit(){
    // this.newSkill.id = this.form.value.skillid;
    this.newSkill.name = this.form.value.skillname;
    this.newSkill.createdBy = this.GetUser();
    this.saveSkill(this.newSkill);
  }

  saveSkill(skill: Skill) {  
   this.skillService.SaveSkill(skill).subscribe((res:any) => {
     console.log(res);
     localStorage.setItem('newSkill', JSON.stringify(this.newSkill));
     console.log(localStorage.setItem('newSkill', JSON.stringify(this.newSkill)));
     this.router.navigate(['/dashboard']);
   });  
  }  

}
