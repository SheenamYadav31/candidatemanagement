import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { DebugElement } from '@angular/core'; 
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router, Routes, RouterModule, ActivatedRoute, Params } from '@angular/router';  
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { BrowserModule, By } from '@angular/platform-browser';
import { of } from 'rxjs';
import {Graduate} from 'src/app/models/Graduate';
import {Skill} from 'src/app/models/Skill';
import { GraduateService } from 'src/app/providers/graduate/graduate.service';    
import { SkillService } from 'src/app/providers/skill/skill.service';    
import { Socialusers } from 'src/app/models/socialusers';
import { AddgradComponent } from 'src/app/components/addgrad/addgrad.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AuthGuardService as AuthGuard } from 'src/app/providers/auth/auth-guard.service';

import { AddskillComponent } from 'src/app/components/addskill/addskill.component';
import { DashboardComponent } from 'src/app/components/dashboard/dashboard.component';
import { NotFoundComponent } from 'src/app/components/not-found/not-found.component';

import { MatInputModule, MatPaginatorModule, MatProgressSpinnerModule, MatSortModule, MatTableModule } from "@angular/material";
import { MatCardModule } from '@angular/material';
import { DisplayskillComponent } from './displayskill.component';

describe('DisplayskillComponent', () => {
  let component: DisplayskillComponent;
  let fixture: ComponentFixture<DisplayskillComponent>;
  let de: DebugElement;
  let el: HTMLElement;
  let authService: AuthGuard;

  const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: {
      title: 'Dashboard Page'
    },
    canActivate: [AuthGuard]  
  },
  {
    path: 'addgrad',
    component: AddgradComponent,
    data:{
      title: 'Add New Graduate'
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'addskill',
    component: AddskillComponent,
    data:{
      title: 'Add New Skill'
    },
    canActivate: [AuthGuard]
  }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayskillComponent, DashboardComponent, AddgradComponent, AddskillComponent ],
      imports : [
        RouterModule.forRoot(routes),
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatInputModule, MatPaginatorModule, MatProgressSpinnerModule, MatSortModule, MatTableModule,MatCardModule
      ],
      providers: [ AuthGuard ]
    })
    .compileComponents().then(() => {
      fixture = TestBed.createComponent(DisplayskillComponent);
      component = fixture.componentInstance;
      de = fixture.debugElement.query(By.css('form'));
      el = de.nativeElement;
    });
    authService = TestBed.get(AuthGuard);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayskillComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
