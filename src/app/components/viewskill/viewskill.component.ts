import { Component, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material';
import {MatTableDataSource} from '@angular/material/table';

import {Skill} from 'src/app/models/Skill';
import { SkillService } from 'src/app/providers/skill/skill.service';    
import { Socialusers } from 'src/app/models/socialusers';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-viewskill',
  templateUrl: './viewskill.component.html',
  styleUrls: ['./viewskill.component.css']
})
export class ViewskillComponent implements OnInit {

  constructor(private skillService: SkillService) { }

  private skills: Skill[] = [];

  displayedColumns  :  string[] = ['id', 'name'];

  dataSource;

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  ngOnInit() {
  	
  	this.skillService.ViewSkill().subscribe((skillList : Skill[]) => {
  		console.log(skillList);
  		this.skills = [...skillList];
  		console.log(this.skills);
  		// this.dataSource  =  skillList;
  		this.dataSource = new MatTableDataSource(skillList);
  		this.ngAfterViewInit();
  		// this.dataSource.sort = this.sort;
  		// this.dataSource.paginator = this.paginator;
  	});
  }

   ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

}
