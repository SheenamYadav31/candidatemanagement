import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router'; 
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material';
import {MatTableDataSource} from '@angular/material/table';

import {Graduate} from 'src/app/models/Graduate';
import { GraduateService } from 'src/app/providers/graduate/graduate.service';    
import { Socialusers } from 'src/app/models/socialusers';

@Component({
  selector: 'app-graduatehistory',
  templateUrl: './graduatehistory.component.html',
  styleUrls: ['./graduatehistory.component.css']
})
export class GraduatehistoryComponent implements OnInit {

  private graduatesList: Graduate[] = [];
  
  displayedColumns  :  string[] = ['id', 'name', 'institute', 'degree', 'location'];

  dataSource;

  graduate: Graduate;

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;


  constructor(private graduateService: GraduateService, private router: Router) { }

  ngOnInit() {
  	this.graduate = JSON.parse(window.localStorage.getItem('selectedGraduate'));
  	this.graduateService.GetGraduateHistory(this.graduate).subscribe((gradList : Graduate[]) => {
  		this.graduatesList = [...gradList];
      console.log(this.graduatesList);
      this.dataSource = new MatTableDataSource(gradList);
      this.ngAfterViewInit();
  	});
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

}
