import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduatehistoryComponent } from './graduatehistory.component';

describe('GraduatehistoryComponent', () => {
  let component: GraduatehistoryComponent;
  let fixture: ComponentFixture<GraduatehistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraduatehistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduatehistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
