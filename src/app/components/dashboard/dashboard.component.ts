import { Component, OnInit } from '@angular/core';
import { SocialLoginModule, AuthServiceConfig, AuthService } from 'angular-6-social-login';  
import { Socialusers } from 'src/app/models/socialusers'  
import { SocialloginService } from 'src/app/providers/sociallogin/sociallogin.service';  
import { Router } from '@angular/router'; 
// import { AddgradComponent } from 'src/components/addgrad/addgrad.component';
// import { EditgradComponent } from 'src/components/editgrad/editgrad.component';
// import { AddskillComponent } from 'src/components/addskill/addskill.component';
// import { TrendsComponent } from 'src/components/trends/trends.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
	socialusers = new Socialusers(); 
  constructor(public OAuth: AuthService, private router: Router) { }
  ngOnInit() {
  	this.socialusers = JSON.parse(localStorage.getItem('socialusers'));  
    console.log(this.socialusers); 
    //if user not loged in redirect to login page;
    if(this.socialusers==null) this.router.navigate['/login'];
  }

  logout() {  
   alert("You will be logged out.");  
    this.OAuth.signOut().then(data => {  
      debugger;
      localStorage.removeItem('token');
      console.log("User Signed Out.")
      this.router.navigate(['/login']);  
    });  
  }  
}