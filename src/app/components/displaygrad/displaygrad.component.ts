import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Graduate} from 'src/app/models/Graduate';

@Component({
  selector: 'app-displaygrad',
  templateUrl: './displaygrad.component.html',
  styleUrls: ['./displaygrad.component.css']
})
export class DisplaygradComponent implements OnInit {

  constructor(private router: Router) { }

  graduate: Graduate;

  ngOnInit() {
    // this.graduate = localStorage.getItem('selectedGraduate');
    this.graduate = JSON.parse(window.localStorage.getItem('selectedGraduate'));
    console.log(this.graduate);
  }

  versionHistory() {
    this.router.navigate(['/history']);
  }

  goToGradViewAll(){
    this.router.navigate(['/viewgrad']);
  }

  goToEdit(){
    this.router.navigate(['/updategrad']);
  }

}
