import { Component, OnInit, ViewChild } from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material';
import {MatTableDataSource} from '@angular/material/table';
import { Router } from '@angular/router'; 
import { ChartsModule } from 'ng2-charts';

import {Graduate} from 'src/app/models/Graduate';
import { GraduateService } from 'src/app/providers/graduate/graduate.service';    
import { Socialusers } from 'src/app/models/socialusers';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-viewgrad',
  templateUrl: './viewgrad.component.html',
  styleUrls: ['./viewgrad.component.css']
})
export class ViewgradComponent implements OnInit {

  constructor(private graduateService: GraduateService, private router: Router) { }

  private graduatesList: Graduate[] = [];
  
  displayedColumns  :  string[] = ['id', 'name', 'institute', 'degree', 'location', 'action'];

  dataSource;

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;


  ngOnInit() {
  	this.graduateService.ViewGraduate().subscribe((gradList : Graduate[]) => {
  		this.graduatesList = [...gradList];
      console.log(this.graduatesList);
      this.dataSource = new MatTableDataSource(gradList);
      this.ngAfterViewInit();
  	});
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  displayRecord(grad){
    console.log(grad);
    localStorage.setItem('selectedGraduate', JSON.stringify(grad));
    this.router.navigate(['/Graduate']);
  }
}
