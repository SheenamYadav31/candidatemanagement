import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';
import { Router, Routes, RouterModule, ActivatedRoute, Params } from '@angular/router';  

import { LoginComponent } from './login.component';
import { DashboardComponent } from 'src/app/components/dashboard/dashboard.component';
import { AuthGuardService as AuthGuard } from 'src/app/providers/auth/auth-guard.service';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authService: AuthGuard;

  const routes: Routes = [
    {
      path: '',
      redirectTo: 'login',
      pathMatch: 'full'  
    },
    {
      path: 'login',
      component: LoginComponent,
      data: {
        title: 'Login Page'
      }  
    },
    {
      path: 'dashboard',
      component: DashboardComponent,
      data: {
        title: 'Dashboard Page'
      },
      canActivate: [AuthGuard]  
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        LoginComponent,
        DashboardComponent
       ],
      imports : [
        RouterModule.forRoot(routes)
      ],
      providers : [
        AuthGuard,
        { provide: APP_BASE_HREF, useValue: '/'}
      ]
    })
    .compileComponents();
    authService = TestBed.get(AuthGuard);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', async(() => {
    expect(component).toBeTruthy();
  }));
});
