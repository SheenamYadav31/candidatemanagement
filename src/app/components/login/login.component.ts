import { Component, OnInit } from '@angular/core';
import { GoogleLoginProvider, AuthService } from 'angular-6-social-login';  
import { SocialLoginModule, AuthServiceConfig } from 'angular-6-social-login';  
import { Socialusers } from 'src/app/models/socialusers'  
import { SocialloginService } from 'src/app/providers/sociallogin/sociallogin.service';  
import { Router, ActivatedRoute, Params } from '@angular/router';  

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  response;
  socialusers = new Socialusers();
  constructor(public OAuth: AuthService,
          private SocialloginService: SocialloginService,
          private router: Router) { }

  ngOnInit() {
  }

  public socialSignIn(socialProvider: string){
  	let socialPlatformProvider;
  	if(socialProvider === 'google'){
  		socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
  	}

  	this.OAuth.signIn(socialPlatformProvider).then(socialusers =>{
  		console.log("socialProvider = google");
  		console.log(socialusers);
  		this.Saveresponse(socialusers);
      // let temp = new Socialusers();
      // temp.name = socialusers.name;
      // temp.image = socialusers.image;
      // temp.email = socialusers.email;
      // temp.provider = socialusers.provider;
      // temp.token = socialusers.token;
      // temp.idToken = socialusers.idToken;
      
      // console.log(temp);
      // localStorage.setItem('socialusers', JSON.stringify(temp));
      // console.log(localStorage.setItem('socialusers', JSON.stringify(temp)));
      // this.router.navigate(['/dashboard']);
      
  	});
  }

  Saveresponse(socialusers: Socialusers){
  	this.SocialloginService.SaveResponse(socialusers).subscribe((res: String) => {
  		console.log(res);
  		this.response = res;
      let temp = new Socialusers();
      temp.id = socialusers.id;
      temp.name = socialusers.name;
      temp.image = socialusers.image;
      temp.email = socialusers.email;
      temp.token = socialusers.token;
      temp.idToken = socialusers.idToken;
      if(!temp.email.includes("@accoliteindia.com")){
        alert("Login with Accolite ID!");
        this.router.navigate(['/login']);
      }
      else{
        console.log(temp);
        localStorage.setItem('socialusers', JSON.stringify(temp));
        console.log(localStorage.setItem('socialusers', JSON.stringify(temp)));
        localStorage.setItem('token', JSON.stringify(temp.token));
        console.log("Token saved.")
    		this.router.navigate(['/dashboard']);
      }
  	});
  }
}
