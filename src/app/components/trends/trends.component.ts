import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { TrendsService } from 'src/app/providers/trends/trends.service';  
import { Router } from '@angular/router'; 
import { DOCUMENT } from '@angular/common';
import * as CanvasJS from 'src/canvasjs.min';
// import { ChartsModule } from 'ng2-charts';

@Component({
  selector: 'app-trends',
  templateUrl: './trends.component.html',
  styleUrls: ['./trends.component.css']
})
export class TrendsComponent implements OnInit {
  LocationsList= [];
  DegreeList= [];
  JoiningYearList = [];
  InstituteList= [];

  LocationChartData = [];
  DegreeChartData = [];
  JoiningYearChartData = [];
  InstituteChartData = [];


  location:boolean = false;

  @ViewChild ('myChart', {static: true}) myChart: ElementRef;

  constructor(private router: Router, private trendsService : TrendsService) { }

  ngOnInit() {
  	this.trendsService.GetLocationList().subscribe((locations : any) => {
  		this.LocationsList = [...locations];
  		console.log(this.LocationsList);
      // console.log(locations);
      // this.LocationsList.splice(this.LocationsList.length-1,1);
      //this.LocationsList--;
  	});

  	this.trendsService.GetDegreeList().subscribe((degrees : any) => {
  		this.DegreeList = [...degrees];
      // this.DegreeList.splice(this.DegreeList.length-1,1);
      console.log(this.DegreeList);
      // this.DegreeList--;
  	});

  	this.trendsService.GetJoiningYearList().subscribe((years : any) => {
  		this.JoiningYearList = [...years];
      // this.JoiningYearList.splice(this.JoiningYearList.length-1,1);
  		console.log(this.JoiningYearList);
      // this.JoiningYearList--;

  	});

  	this.trendsService.GetInstituteList().subscribe((institute : any) => {
  		this.InstituteList = [...institute];
      // this.InstituteList.splice(this.InstituteList.length-1,1);
  		console.log(this.InstituteList);
      // this.InstituteList--;
  	});

  }

  getLocationChartData(){
    this.LocationChartData = [];
  	var b = new Promise((resolve, reject) => {
  		this.LocationsList.forEach((value, index, array) => {
  			this.trendsService.GetGradCountByLocation(value).subscribe((count : any) => {
			this.LocationChartData.push(count);
			if (index === array.length -1) resolve();
			});
			// console.log(this.LocationChartData);
  		});
  	});

  	b.then(()=> {
  		console.log(this.LocationChartData)
  		this.displayLocationChart();
  	});
  }

  getDegreeChartData(){
    this.DegreeChartData=[];
  	var b = new Promise((resolve, reject) => {
  		this.DegreeList.forEach((value, index, array) => {
  			this.trendsService.GetGradCountByDegree(value).subscribe((count : any) => {
			this.DegreeChartData.push(count);
			if (index === array.length -1) resolve();
			});
			// console.log(this.LocationChartData);
  		});
  	});

  	b.then(()=> {
  		console.log(this.DegreeChartData);
  		this.displayDegreeChart();
  	});
  }

   getJoiningYearChartData(){
    this.JoiningYearChartData = [];
  	var b = new Promise((resolve, reject) => {
  		this.JoiningYearList.forEach((value, index, array) => {
  			this.trendsService.GetGradCountByJoiningYear(value).subscribe((count : any) => {
			this.JoiningYearChartData.push(count);
			if (index === array.length -1) resolve();
			});
			// console.log(this.LocationChartData);
  		});
  	});

  	b.then(()=> {
  		console.log(this.JoiningYearChartData);
  		this.displayJoiningYearChart();
  	});
  }

  getInstituteChartData(){
    this.InstituteChartData = [];
  	var b = new Promise((resolve, reject) => {
  		this.InstituteList.forEach((value, index, array) => {
  			this.trendsService.GetGradCountByInstitute(value).subscribe((count : any) => {
			this.InstituteChartData.push(count);
			if (index === array.length -1) resolve();
			});
			// console.log(this.LocationChartData);
  		});
  	});

  	b.then(()=> {
  		console.log(this.InstituteChartData);
  		this.displayInstituteChart();
  	});
  }

   displayLocationChart(){
  	var dataPoints = [];

  	for ( var j = 0; j < this.LocationsList.length; j++ ) {		  
  		// console.log({ y: this.LocationChartData[j], label: this.LocationsList[j]});
  		if(!this.LocationChartData[j]) this.LocationChartData[j]=0;
		dataPoints.push({ y: this.LocationChartData[j], label: this.LocationsList[j]});
	}
  	let chart = new CanvasJS.Chart("chartContainer", {
		zoomEnabled: true,
		animationEnabled: true,
		exportEnabled: true,
		title: {
			text: "Graduates per Location"
		},
		subtitles:[{
			text: "Try Zooming and Panning"
		}],
		data: [
		{
			type: "column",                
			dataPoints: dataPoints
		}]
	});
	chart.render();
  }

  displayDegreeChart(){
  	let dataPoints = [];
	  let dpsLength = 0;
	for ( var k = 0; k < this.DegreeList.length; k++ ) {		  
		dataPoints.push({ y: this.DegreeChartData[k], name: this.DegreeList[k]});
		console.log(dataPoints[k]);
	}
	let chart = new CanvasJS.Chart("chartContainer", {
		theme: "light2",
		animationEnabled: true,
		exportEnabled: true,
		title:{
			text: "Graduates Per Degree"
		},
		data: [{
			type: "pie",
			showInLegend: true,
			toolTipContent: "<b>{name}</b>: {y} (#percent%)",
			indexLabel: "{name} - #percent%",
			dataPoints: dataPoints
		}]
	});
	chart.render();
  }

  displayJoiningYearChart(){
  let dataPoints = [];
	for ( var l = 0; l < this.JoiningYearList.length; l++ ) {		  
		dataPoints.push({ y: this.JoiningYearChartData[l], label: this.JoiningYearList[l]});
		console.log(dataPoints[l]);
	}
	let chart = new CanvasJS.Chart("chartContainer",{
		exportEnabled: true,
		title:{
			text:"Graduates Joining per Year"
		},
		data: [{
			type: "spline",
			dataPoints : dataPoints,
		}]
	});

  }

  displayInstituteChart(){
  	let dataPoints = [];
  	for ( var m = 0; m < this.InstituteList.length; m++ ) {		  
		dataPoints.push({ y: this.InstituteChartData[m], name: this.InstituteList[m]});
		console.log(dataPoints[m]);
	}
  	let chart = new CanvasJS.Chart("chartContainer", {
		theme: "light2",
		animationEnabled: true,
		exportEnabled: true,
		title:{
			text: "Graduates Per Institute"
		},
		data: [{
			type: "pie",
			showInLegend: true,
			toolTipContent: "<b>{name}</b>: {y} (#percent%)",
			indexLabel: "{name} - #percent%",
			dataPoints: dataPoints
		}]
	});
	chart.render();
  }
}