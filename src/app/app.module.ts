import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { GoogleLoginProvider, AuthService } from 'angular-6-social-login';
import { SocialLoginModule, AuthServiceConfig } from 'angular-6-social-login';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { MatInputModule, MatPaginatorModule, MatProgressSpinnerModule, MatSortModule, MatTableModule } from "@angular/material";
import { MatCardModule } from '@angular/material';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ChartsModule } from 'ng2-charts';
// import { MdCardModule } from '@angular/material';
// import { MatInputModule, MatDatepickerModule, MatNativeDateModule} from '@angular/material';
// import { MatFormFieldModule } from '@angular/material/form-field';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AddgradComponent } from './components/addgrad/addgrad.component';
import { EditgradComponent } from './components/editgrad/editgrad.component';
import { AddskillComponent } from './components/addskill/addskill.component';
import { TrendsComponent } from './components/trends/trends.component';
import { ViewgradComponent } from './components/viewgrad/viewgrad.component';
import { ViewskillComponent } from './components/viewskill/viewskill.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DisplaygradComponent } from './components/displaygrad/displaygrad.component';
import { DisplayskillComponent } from './components/displayskill/displayskill.component';

import { AuthGuardService } from './providers/auth/auth-guard.service';
import { GraduatehistoryComponent } from './components/graduatehistory/graduatehistory.component';

//client id for google login
const google_auth_id:string = "462676608760-cj2oi3j2hc7n07g41i67oe85rdclbn6c.apps.googleusercontent.com"

// const JWT_Module_Options: JwtModuleOptions = {
//     config: {
//         tokenGetter: ()=>{
//           return localStorage.getItem("token");
//         },
//         whitelistedDomains: ["localhost:4200"]
//     }
// };

export function socialConfigs() {  
  const config = new AuthServiceConfig(  
    [   
      {  
        id: GoogleLoginProvider.PROVIDER_ID,  
        provider: new GoogleLoginProvider(google_auth_id)  
      }  
    ]  
  );  
  return config;  
}  

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    NotFoundComponent,
    AddgradComponent,
    EditgradComponent,
    AddskillComponent,
    TrendsComponent,
    ViewgradComponent,
    ViewskillComponent,
    DisplaygradComponent,
    DisplayskillComponent,
    GraduatehistoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatCardModule,
    AngularFontAwesomeModule,
    ChartsModule
  ],
  providers: [
    AuthGuardService,
    AuthService,
    {
      provide: AuthServiceConfig,
      useFactory: socialConfigs
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
